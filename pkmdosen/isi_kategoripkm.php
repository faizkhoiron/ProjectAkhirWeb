<?php
    include_once 'top.php';
    require_once 'db/class_kategoripkm.php';
    
?>
<h2>Daftar Kategori PKM</h2>

<?php
    $obj_kategori = new Kategori();
    $rows = $obj_kategori->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->

<script language="JavaScript">
 $(document).ready(function() {
 $('#example').DataTable();
 } );
</script>

<table id="example" class="table table-striped table-bordered">

    <thead>
    <tr class="success">
        <th>Id</th>
        <th>Nama</th>

        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nama'].'</td>';

        echo '<td><a href="view_kategoripkm.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_kategoripkm.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>

<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_kategoripkm.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Kategori
    </a>
</div>
<br>

<?php
      include_once 'bottom.php';
?>