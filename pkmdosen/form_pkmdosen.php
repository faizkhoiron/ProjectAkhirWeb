<?php
	include_once 'top.php';
      //panggil file yang melakukan operasi db

	require_once 'db/class_pkmdosen.php';

	$obj_pkm = new Pkm();
    //buat variabel untuk memanggil class

	$_idedit = $_GET['id'];
    //buat variabel utk menyimpan id

	if(!empty($_idedit)){
		$data = $obj_pkm->findByID($_idedit);
    //buat pengecekan apakah datanya ada atau tidak

	}else{
		$data = [];
	}
?>
<script src="js/form_validasi_pkmdosen.js"></script>

<form class="form-horizontal" method="POST" name="form_pkmdosen" action="proses_pkmdosen.php">
<fieldset><fieldset>

<!-- Form Name -->
<legend>Form PKM Dosen</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tanggal_mulai">Tanggal Mulai</label>  
  <div class="col-md-4">
  <input id="tanggal_mulai" name="tanggal_mulai" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['tanggal_mulai']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tanggal_akhir">Tanggal Akhir</label>  
  <div class="col-md-4">
  <input id="tanggal_akhir" name="tanggal_akhir" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['tanggal_akhir']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">Judul</label>  
  <div class="col-md-4">
  <input id="judul" name="judul" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['judul']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tempat">Tempat</label>  
  <div class="col-md-4">
  <input id="tempat" name="tempat" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['tempat']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="biaya">Biaya</label>  
  <div class="col-md-4">
  <input id="biaya" name="biaya" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['biaya']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="dosen_id">Id Dosen</label>  
  <div class="col-md-4">
  <input id="dosen_id" name="dosen_id" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['dosen_id']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="semester">Semester</label>  
  <div class="col-md-4">
  <input id="semester" name="semester" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['semester']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kategori_pkm_id">Id Kategori PKM</label>  
  <div class="col-md-4">
  <input id="kategori_pkm_id" name="kategori_pkm_id" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['kategori_pkm_id']?>">
    
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">

  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>

    <?php

    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>

    <?php

    }?>
<script src="js/form_validasi_pkmdosen.js"></script>

  </div>
</div>

</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
