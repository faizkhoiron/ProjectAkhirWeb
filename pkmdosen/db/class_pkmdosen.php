<?php
	include_once 'top.php';

/*
	mysql> desc pkm_dosen;
	+-----------------+--------------+------+-----+---------+----------------+
	| Field           | Type         | Null | Key | Default | Extra          |
	+-----------------+--------------+------+-----+---------+----------------+
	| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
	| tanggal_mulai   | date         | YES  |     | NULL    |                |
	| tanggal_akhir   | date         | YES  |     | NULL    |                |
	| judul           | text         | YES  |     | NULL    |                |
	| tempat          | varchar(100) | YES  |     | NULL    |                |
	| biaya           | double       | YES  |     | NULL    |                |
	| dosen_id        | int(11)      | NO   | MUL | NULL    |                |
	| semester        | int(11)      | YES  |     | NULL    |                |
	| kategori_pkm_id | int(11)      | NO   | MUL | NULL    |                |
	+-----------------+--------------+------+-----+---------+----------------+
	9 rows in set (0,00 sec)

*/

require_once "DAO.php";

class Pkm extends DAO{

	public function __construct(){
		parent::__construct("pkm_dosen");
	}

	public function simpan($data){
		$sql = "INSERT INTO ".$this->tableName.
		" (id, tanggal_mulai, tanggal_akhir, judul, tempat, biaya, dosen_id, semester, kategori_pkm_id)".
		" VALUES (default,?,?,?,?,?,?,?,?)";

		$ps = $this->koneksi->prepare($sql);
		$ps->execute($data);
			return $ps->rowCount();
	}

	public function ubah($data){
		$sql = "UPDATE ".$this->tableName.
		" SET tanggal_mulai=?, tanggal_akhir=?, judul=?, tempat=?, biaya=?, dosen_id=?, semester=?, kategori_pkm_id=?".
		" WHERE id=?";

		$ps = $this->koneksi->prepare($sql);
		$ps->execute($data);
			return $ps->rowCount();
	}

        public function getStatistik(){
            $sql = "SELECT a.nama,COUNT(b.id) as jumlah from prodi a
                    LEFT JOIN dosen b ON a.id=b.prodi_id
                    GROUP BY a.nama";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute();
                return $ps->fetchAll();
        }


}
?>