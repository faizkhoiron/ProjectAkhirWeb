$(function(){
	$("form[name='form_pkmdosen']").validate({
		rules:{
			tanggal_mulai:"required",
			tanggal_akhir:"required",
			judul:"required",
			tempat:"required",
			biaya:"required",
			dosen_id:"required",
			semester:"required",
			kategori_pkm_id:"required",

		},
		messages: {
			tanggal_mulai:"Wajib diisi",
			tanggal_akhir:"Wajib diisi",
			judul:"Wajib diisi",
			tempat:"Wajib diisi",
			biaya:"Wajib diisi",
			dosen_id:"Wajib diisi",
			semester:"Wajib diisi",
			kategori_pkm_id:"Wajib diisi",
		},
		submitHandler: function(form){
			form.submit();
		}
	});
});