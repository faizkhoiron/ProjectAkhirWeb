<?php
    include_once 'top.php';
        //panggil file untuk operasi db

    require_once 'db/class_pkmdosen.php';
    $obj_pkm = new Pkm();
        //buat variabel utk menyimpan id

    $_id = $_GET['id'];
        //buat variabel untuk mengambil id

    $data = $obj_pkm->findByID($_id);
?>


<div class="row">
	<div class="col-md-12">
 		<div class="panel panel-default">
 			<div class="panel-heading">
 				<h3 class="panel-title">View PKM Dosen</h3>
 			</div>
 	
 		<div class="panel-body">
 			<table class="table">
			 <tr>
 			 	<td class="active">Id</td>
 			 	<td>:</td>
 			 	<td> <?php echo $data['id']?> </td>
 			 </tr>
  			 
  		 <tr>
 				<td class="active">Tanggal Mulai</td>
 				<td>:</td>
 				<td><?php echo $data['tanggal_mulai']?></td>
			 </tr>

       <tr>
        <td class="active">Tanggal Akhir</td>
        <td>:</td>
        <td><?php echo $data['tanggal_akhir']?></td>
       </tr>

       <tr>
        <td class="active">Judul</td>
        <td>:</td>
        <td><?php echo $data['judul']?></td>
       </tr>

       <tr>
        <td class="active">Tempat</td>
        <td>:</td>
        <td><?php echo $data['tempat']?></td>
       </tr>

       <tr>
        <td class="active">Biaya</td>
        <td>:</td>
        <td><?php echo $data['biaya']?></td>
       </tr>

       <tr>
        <td class="active">Id Dosen</td>
        <td>:</td>
        <td><?php echo $data['dosen_id']?></td>
       </tr>

       <tr>
        <td class="active">Semester</td>
        <td>:</td>
        <td><?php echo $data['semester']?></td>
       </tr>

       <tr>
        <td class="active">Id Kategori Pkm</td>
        <td>:</td>
        <td><?php echo $data['kategori_pkm_id']?></td>
       </tr>

 			 
 			</table>
 		</div>
 
 	<div class="panel-footer">
 		<a class="btn icon-btn btn-success" href="form_pkmdosen.php">
 			<span class="glyphicon btn-glyphicon glyphicon-plus imgcircle text-success"></span>
		 Tambah Pkm
		</a>
 
             	  </div>
 			</div>
	  </div>
</div>


<?php
include_once 'bottom.php';
?>