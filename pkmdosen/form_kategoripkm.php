<?php
	include_once 'top.php';
      //panggil file yang melakukan operasi db

	require_once 'db/class_kategoripkm.php';

	$obj_kategori = new Kategori();
    //buat variabel untuk memanggil class

	$_idedit = $_GET['id'];
    //buat variabel utk menyimpan id

	if(!empty($_idedit)){
		$data = $obj_kategori->findByID($_idedit);
    //buat pengecekan apakah datanya ada atau tidak

	}else{
		$data = [];
	}
?>
<script src="js/form_validasi_kategoripkm.js"></script>

<form class="form-horizontal" method="POST" name="form_kategoripkm" action="proses_kategoripkm.php">
<fieldset><fieldset>

<!-- Form Name -->
<legend>Form Kategori PKM</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="id">id</label>  
  <div class="col-md-4">
  <input id="id" name="id" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['id']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama</label>  
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['nama']?>">
    
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">

  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>

    <?php

    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>

    <?php

    }?>
<script src="js/form_validasi_kategoripkm.js"></script>

  </div>
</div>

</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
