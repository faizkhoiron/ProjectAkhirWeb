<?php
    include_once 'top.php';
    require_once 'db/class_pkmdosen.php';
    
?>
<h2>Daftar PKM Dosen</h2>

<?php
    $obj_pkm = new Pkm();
    $rows = $obj_pkm->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->

<script language="JavaScript">
 $(document).ready(function() {
 $('#example').DataTable();
 } );
</script>

<table id="example" class="table table-striped table-bordered">

    <thead>
    <tr class="success">
        <th>Id</th>
        <th>Tanggal Mulai</th>
        <th>Tanggal Akhir</th>
        <th>Judul</th>
        <th>Tempat</th>
        <th>Biaya</th>
        <th>Id Dosen</th>
        <th>Semester</th>
        <th>Id Kategori PKM</th>

        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['tanggal_mulai'].'</td>';
        echo '<td>'.$row['tanggal_akhir'].'</td>';
        echo '<td>'.$row['judul'].'</td>';
        echo '<td>'.$row['tempat'].'</td>';
        echo '<td>'.$row['biaya'].'</td>';
        echo '<td>'.$row['dosen_id'].'</td>';
        echo '<td>'.$row['semester'].'</td>';
        echo '<td>'.$row['kategori_pkm_id'].'</td>';

        echo '<td><a href="view_pkmdosen.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_pkmdosen.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>

<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_pkmdosen.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Pkm
    </a>
</div>
<br>

<?php
      include_once 'bottom.php';
?>