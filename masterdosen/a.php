<?php
    include_once 'top.php';
?>
<section>
    <div class="container" align="center">
        <div class="row">

            <div class="col-md-12 col-sm-12">
                <h1 class="wow fadeInUp" data-wow-delay="1.6s">Selamat Datang </h1><br>
                <img src="images/sttnf.jpg">
                <h3 class="wow bounceIn" data-wow-delay="0.9s">Sekolah Tinggi Teknologi Terpadu Nurul Fikri</h3>
                <br>
                
                <a href="b.php" class="btn btn-lg btn-success smoothScroll wow fadeInUp hidden-xs" data-wow-delay="2.3s">Modul Master Dosen</a>
             
                <a href="/pkmdosen/c.php" class="btn btn-lg btn-success smoothScroll wow fadeInUp hidden-xs" data-wow-delay="2.3s">Modul PKM Dosen </a>
               
            </div>
        </div>
    </div>
</section>
<?php
    include_once 'bottom.php';
?>