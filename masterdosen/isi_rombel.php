<?php
    include_once 'top.php';

    
    require_once 'db/class_rombel.php';
    
?>
<h2>Daftar Rombel</h2>

<?php
    $obj_rombel = new Rombel();
    $rows = $obj_rombel->getAll();
?>

<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->

<script language="JavaScript">
 $(document).ready(function() {
 $('#example').DataTable();
 } );
</script>

<table id="example" class="table table-striped table-bordered">


    <thead>
    <tr class="success">
    	<th>Id</th>
        <th>Nama Rombel</th>
        <th>Mahasiswa Angkatan</th>
        <th>Dosen Pembimbing</th>
        <th>Prodi</th>
        
        <th>Action</th>
    </tr>
    
    </thead>
    <tbody>
        
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td>'.$row['mhs_angkatan'].'</td>';
        echo '<td>'.$row['dosen_pa'].'</td>';
        echo '<td>'.$row['prodi_id'].'</td>';
        
        echo '<td><a href="view_rombel.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_rombel.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>

<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_rombel.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Rombel
    </a>
</div>
<br>

<?php
    include_once 'bottom.php';
?>