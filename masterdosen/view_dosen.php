<?php
    include_once 'top.php';
        //panggil file untuk operasi db

    require_once 'db/class_dosen.php';
    $obj_dosen = new Dosen();
        //buat variabel utk menyimpan id

    $_id = $_GET['id'];
        //buat variabel untuk mengambil id

    $data = $obj_dosen->findByID($_id);
?>


<div class="row">
	<div class="col-md-12">
 		<div class="panel panel-default">
 			<div class="panel-heading">
 				<h3 class="panel-title">View Dosen</h3>
 			</div>
 	
 		<div class="panel-body">
 			<table class="table">
			 <tr>
 			 	<td class="active">Nidn</td>
 			 	<td>:</td>
 			 	<td> <?php echo $data['nidn']?> </td>
 			 </tr>
  			 
  			 <tr>
 				<td class="active">Nama Lengkap</td>
 				<td>:</td>
 				<td><?php echo $data['nama']?></td>
			 </tr>
 
 			 <tr>
 			  <td class="active">Gelar Belakang</td>
 			  <td>:</td>
 			  <td><?php echo $data['gelar_belakang']?></td>
 			 </tr>
 
 			 <tr>
 			  <td class="active">Jenis Kelamin</td>
 			  <td>:</td>
 			  <td><?php echo $data['jk']?></td>
 			 </tr>

 			 <tr>
 			  <td class="active">Prodi</td>
 			  <td>:</td>
 			  <td><?php echo $data['prodi_id']?></td>
 			 </tr>

 			 <tr>
 			  <td class="active">Jabatan</td>
 			  <td>:</td>
 			  <td><?php echo $data['jabatan_id']?></td>
 			 </tr>

 			 <tr>
 			  <td class="active">Pendidikan Akhir</td>
 			  <td>:</td>
 			  <td><?php echo $data['pend_akhir']?></td>
 			 </tr>

 			 
 			</table>
 		</div>
 
 	<div class="panel-footer">
 		<a class="btn icon-btn btn-success" href="form_dosen.php">
 			<span class="glyphicon btn-glyphicon glyphicon-plus imgcircle text-success"></span>
		 Tambah Dosen
		</a>
 
             	  </div>
 			</div>
	  </div>
</div>


<?php
include_once 'bottom.php';
?>