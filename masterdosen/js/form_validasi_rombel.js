	$(function() { //buat fungsi

		$("form[name='form_rombel']").validate ({
			rules: {
				nama: "required"
				mhs_angkatan: "required",
				dosen_pa: "required",
				prodi_id: "required",
			},
			messages: {
				nama: "Wajib diisi"
				mhs_angkatan: "Wajib diisi",
				dosen_pa: "Wajib diisi",
				prodi_id: "Wajib diisi",
			},
			submitHandler: function(form){
				form.submit();
			}
		});
	});
 
