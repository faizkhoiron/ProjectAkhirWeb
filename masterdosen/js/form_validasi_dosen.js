$(function(){

	$("form[name='form_dosen']").validate({
		rules: {
			nidn:{
				required:true,
				maxlenght:10,
			},
			nama:"required",
			gelar_belakang:"required",
			jk:"required",
			prodi_id:"required",
			jabatan_id:"required",
			pend_akhir:"required",
		},
		messages:{
			nidn:{
				required:"Wajib diisi",
				maxlenght:"Maximum 10 angka",
			},
			nama:"Wajib diisi",
			gelar_belakang:"Wajib diisi",
			jk:"Wajib diisi",
			prodi_id:"Wajib diisi",
			jabatan_id:"Wajib diisi",
			pend_akhir:"Wajib diisi",
		},
		submitHandler : function(form){
			form.submit();
		}
	});
});