$(function(){
	$("form[name='form_prodi']").validate({
		rules:{
			kode:{
				required:true,
				maxlenght: 3,
			},
			nama:"required",
		},
		messages: {
			kode:{
				required:"Wajib diisi",
				maxlenght:"cuma 3"
			},
			nama:"Wajib diisi",
		},
		submitHandler: function)(form){
			form.submit();
		}
	});
});