<?php
    include_once 'top.php';
        //panggil file untuk operasi db

    require_once 'db/class_rombel.php';
    $obj_rombel = new Rombel();
        //buat variabel utk menyimpan id

    $_id = $_GET['id'];
        //buat variabel untuk mengambil id

    $data = $obj_rombel->findByID($_id);
?>


<div class="row">
	<div class="col-md-12">
 		<div class="panel panel-default">
 			<div class="panel-heading">
 				<h3 class="panel-title">View Rombel</h3>
 			</div>
 	
 		<div class="panel-body">
 			<table class="table">
			 <tr>
 			 	<td class="active">Nama Rombel</td>
 			 	<td>:</td>
 			 	<td> <?php echo $data['nama']?> </td>
 			 </tr>
  			 
  			 <tr>
 				<td class="active">Mahasiswa Angkatan</td>
 				<td>:</td>
 				<td><?php echo $data['mhs_angkatan']?></td>
			 </tr>
 
 			 <tr>
 			  <td class="active">Dosen Pembimbing</td>
 			  <td>:</td>
 			  <td><?php echo $data['dosen_pa']?></td>
 			 </tr>
 
 			 <tr>
 			  <td class="active">Prodi</td>
 			  <td>:</td>
 			  <td><?php echo $data['prodi_id']?></td>
 			 </tr>

 			</table>
 		</div>
 
 	<div class="panel-footer">
 		<a class="btn icon-btn btn-success" href="form_rombel.php">
 			<span class="glyphicon btn-glyphicon glyphicon-plus imgcircle text-success"></span>
		 Tambah Rombel
		</a>
 
             	  </div>
 			</div>
	  </div>
</div>


<?php
include_once 'bottom.php';
?>