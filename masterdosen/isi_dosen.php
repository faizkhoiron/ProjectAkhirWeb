<?php
    include_once 'top.php';
    require_once 'db/class_dosen.php';
    
?>
<h2>Daftar Dosen</h2>

<?php
    $obj_dosen = new Dosen();
    $rows = $obj_dosen->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->

<script language="JavaScript">
 $(document).ready(function() {
 $('#example').DataTable();
 } );
</script>

<table id="example" class="table table-striped table-bordered">

    <thead>
    <tr class="success">
        <th>No</th>
        <th>Nidn</th>
        <th>Nama Lengkap</th>
        <th>Gelar Belakang</th>
        <th>Jenis Kelamin</th>
        <th>Prodi</th>
        <th>Jabatan</th>
        <th>Pendidikan Akhir</th>

        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nidn'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td>'.$row['gelar_belakang'].'</td>';
        echo '<td>'.$row['jk'].'</td>';
        echo '<td>'.$row['prodi_id'].'</td>';
        echo '<td>'.$row['jabatan_id'].'</td>';
        echo '<td>'.$row['pend_akhir'].'</td>';

        echo '<td><a href="view_dosen.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_dosen.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>

<div>
    <a class="btn icon-btn btn" href="grafik_dosen.php">Tampilkan Grafik</a>
</div><br>

<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_dosen.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Dosen
    </a>
</div>
<br>

<?php
      include_once 'bottom.php';
?>